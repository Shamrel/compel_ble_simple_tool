# STM32CubeWB - простой инструмент для создания прототипа BLE-приложения на STM32WB55
*Исходные коды и проект к статье* 

## Платформа:
* Микроконтроллер: **STM32WB55RGV**
* Плата: **Nucleo-68** из набора [P-NUCLEO-WB55](https://www.st.com/content/st_com/en/products/evaluation-tools/product-evaluation-tools/stm32-nucleo-expansion-boards/p-nucleo-wb55.html) 

## IDE:
* Atollic TrueSTUDIO версии 9.3.0  
* STM32CobeMX версии 5.4.0  
* STM32CubeWB версии 1.3.0 